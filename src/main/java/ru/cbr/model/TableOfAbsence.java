package ru.cbr.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Roman Velikoselsky
 */

@Entity
@Table (name = "table_of_absence", schema = "absence")
public class TableOfAbsence {

    @Id
    @Column (name = "absence_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int absenceId;

    @Column (name = "user_in_absence_id", nullable = false, insertable = false, updatable = false)
    private int userId;

    @ManyToOne
    @JoinColumn(name = "user_in_absence_id")
    private User user;

    @Column (name = "time_of_absence", nullable = false)
    private long timeOfAbsence;

    @Column (name = "date_of_absence", nullable = false)
    private Date dateOfAbsence;

    @Column (name = "reason_of_absence", nullable = false)
    private String reasonOfAbsence;

    public int getAbsenceId() {
        return absenceId;
    }

    public void setAbsenceId(int absenceId) {
        this.absenceId = absenceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getTimeOfAbsence() {
        return timeOfAbsence;
    }

    public void setTimeOfAbsence(long timeOfAbsence) {
        this.timeOfAbsence = timeOfAbsence;
    }

    public Date getDateOfAbsence() {
        return dateOfAbsence;
    }

    public void setDateOfAbsence(Date dateOfAbsence) {
        this.dateOfAbsence = dateOfAbsence;
    }

    public String getReasonOfAbsence() {
        return reasonOfAbsence;
    }

    public void setReasonOfAbsence(String reasonOfAbsence) {
        this.reasonOfAbsence = reasonOfAbsence;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TableOfAbsence{" +
                "absenceId=" + absenceId +
                ", userId=" + userId +
                ", timeOfAbsence=" + timeOfAbsence +
                ", dateOfAbsence=" + dateOfAbsence +
                ", reasonOfAbsence='" + reasonOfAbsence + '\'' +
                '}';
    }
}
