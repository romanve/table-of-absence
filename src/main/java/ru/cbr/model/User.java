package ru.cbr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Roman Velikoselsky
 */

@Entity
@Table (name = "user_position", schema = "absence")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {

    @Id
    @Column (name = "user_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @JsonProperty ("id")
    private int id;

    @Column (name = "user_full_name", nullable = false)
    @JsonProperty ("fullName")
    private String fullName;

    @Column (name = "user_position", nullable = false)
    @JsonProperty ("position")
    private String position;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<TableOfAbsence> tableOfAbsences = new HashSet<TableOfAbsence>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
