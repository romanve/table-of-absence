package ru.cbr.service;

import ru.cbr.dao.TableOfAbsenceDao;
import ru.cbr.dao.UserPositionDao;
import ru.cbr.model.TableOfAbsence;
import ru.cbr.model.User;

import java.util.List;

/**
 * @author Roman Velikoselsky
 */

public interface TableOfAbsenceService extends UserPositionDao, TableOfAbsenceDao {
    boolean addAbsence(TableOfAbsence tableOfAbsence);

    boolean updateAbsence(TableOfAbsence tableOfAbsence);

    boolean removeAbsence(int id);

    List getAbsenceListByUser(User user);

    List getAbsenceList();

    boolean addUser(User user);

    boolean updateUser(User user);

    boolean removeUser(int id);

    TableOfAbsence getAbsenceById(int id);

    User getUserById(int id);

    User getUserByName(String fullName);

    List getUsersList();

    List getFilteredUsersList(User user);

    List getFilteredTableOfAbsenceList(TableOfAbsence tableOfAbsence, User user, String position);
}
