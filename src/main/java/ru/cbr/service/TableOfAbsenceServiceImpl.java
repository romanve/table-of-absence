package ru.cbr.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.cbr.dao.TableOfAbsenceDao;
import ru.cbr.dao.UserPositionDao;
import ru.cbr.model.TableOfAbsence;
import ru.cbr.model.User;

import java.util.List;

/**
 * @author Roman Velikoselsky
 */

@Service
public class TableOfAbsenceServiceImpl implements TableOfAbsenceService {
    private UserPositionDao userPositionDao;
    private TableOfAbsenceDao tableOfAbsenceDao;

    @Transactional
    public boolean addAbsence(TableOfAbsence tableOfAbsence) {
        return tableOfAbsenceDao.addAbsence(tableOfAbsence);
    }

    @Transactional
    public boolean updateAbsence(TableOfAbsence tableOfAbsence) {
        return tableOfAbsenceDao.updateAbsence(tableOfAbsence);
    }

    @Transactional
    public boolean removeAbsence(int id) {
        return tableOfAbsenceDao.removeAbsence(id);
    }

    @Transactional
    public List getAbsenceListByUser(User user) {
        return tableOfAbsenceDao.getAbsenceListByUser(user);
    }

    @Transactional
    public List getAbsenceList() {
        return tableOfAbsenceDao.getAbsenceList();
    }

    @Transactional
    public boolean addUser(User user) {
        return userPositionDao.addUser(user);
    }

    @Transactional
    public boolean updateUser(User user) {
        return userPositionDao.updateUser(user);
    }

    @Transactional
    public boolean removeUser(int id) {
        return userPositionDao.removeUser(id);
    }

    public TableOfAbsence getAbsenceById(int id) {
        return tableOfAbsenceDao.getAbsenceById(id);
    }

    @Transactional
    public User getUserById(int id) {
        return userPositionDao.getUserById(id);
    }

    @Transactional
    public User getUserByName(String fullName) {
        return userPositionDao.getUserByName(fullName);
    }

    @Transactional
    public List getUsersList() {
        return userPositionDao.getUsersList();
    }

    @Transactional
    public List getFilteredUsersList(User user) {
        return userPositionDao.getFilteredUsersList(user);
    }

    @Transactional
    public List getFilteredTableOfAbsenceList(TableOfAbsence tableOfAbsence, User user, String position) {
        return tableOfAbsenceDao.getFilteredTableOfAbsenceList(tableOfAbsence, user, position);
    }

    @Transactional
    public void setUserPositionDao(UserPositionDao userPositionDao) {
        this.userPositionDao = userPositionDao;
    }

    @Transactional
    public void setTableOfAbsenceDao(TableOfAbsenceDao tableOfAbsenceDao) {
        this.tableOfAbsenceDao = tableOfAbsenceDao;
    }
}
