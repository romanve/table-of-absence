package ru.cbr.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ru.cbr.model.TableOfAbsence;
import ru.cbr.model.User;
import ru.cbr.service.TableOfAbsenceService;

import java.util.List;

/**
 * @author Roman Velikoselsky
 */

@RestController
public class TableOfAbsenceController {
    private TableOfAbsenceService tableOfAbsenceService;

    @RequestMapping(value = "/add-absence", method = RequestMethod.POST)
    public ResponseEntity<TableOfAbsence> addAbsence(@RequestBody TableOfAbsence tableOfAbsence) {
        if (tableOfAbsenceService.addAbsence(tableOfAbsence)) {
            return new ResponseEntity<TableOfAbsence>(tableOfAbsence, HttpStatus.OK);
        } else {
            return new ResponseEntity<TableOfAbsence>(tableOfAbsence, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/update-absence/{id}", method = RequestMethod.POST)
    public ResponseEntity<TableOfAbsence> updateAbsence(@PathVariable("id") int id,
                                                        @RequestBody TableOfAbsence tableOfAbsence) {
        TableOfAbsence updatableAbsence = tableOfAbsenceService.getAbsenceById(id);

        if (updatableAbsence != null) {
            updatableAbsence.setDateOfAbsence(tableOfAbsence.getDateOfAbsence());
            updatableAbsence.setReasonOfAbsence(tableOfAbsence.getReasonOfAbsence());
            updatableAbsence.setTimeOfAbsence(tableOfAbsence.getTimeOfAbsence());
            updatableAbsence.setUserId(updatableAbsence.getUserId());

            tableOfAbsenceService.updateAbsence(updatableAbsence);

            return new ResponseEntity<TableOfAbsence>(HttpStatus.OK);
        } else {
            return new ResponseEntity<TableOfAbsence>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/remove-absence/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<TableOfAbsence> removeAbsence(@PathVariable("id") int id) {
        if (tableOfAbsenceService.removeAbsence(id)) {
            return new ResponseEntity<TableOfAbsence>(HttpStatus.OK);
        } else {
            return new ResponseEntity<TableOfAbsence>(HttpStatus.NO_CONTENT);
        }
    }


    @RequestMapping(value = "/get-absence-list", method = RequestMethod.GET)
    public ResponseEntity<List> getListOfAbsence() {
        List absenceList = tableOfAbsenceService.getAbsenceList();

        if (absenceList.isEmpty()) {
            return new ResponseEntity<List>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List>(absenceList, HttpStatus.OK);
    }

    @RequestMapping(value = "/get-filtered-absence-list/{userName}&{position}", method = RequestMethod.POST)
    public ResponseEntity<List> getFilteredListOfAbsence(@PathVariable String userName,
                                                         @PathVariable String position,
                                                         @RequestBody TableOfAbsence tableOfAbsence) {
        User user = null;

        if (!StringUtils.isEmpty(userName) && !"undefined".equals(userName)) {
            user = tableOfAbsenceService.getUserByName(userName);
        }

        List absenceList = tableOfAbsenceService.getFilteredTableOfAbsenceList(tableOfAbsence, user, position);

        if (absenceList.isEmpty()) {
            return new ResponseEntity<List>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List>(absenceList, HttpStatus.OK);
    }

    @RequestMapping(value = "/get-user-by-id/{id}", method = RequestMethod.POST)
    public ResponseEntity<User> getUserById(@PathVariable("id") int id) {
        User user = tableOfAbsenceService.getUserById(id);

        if (user != null) {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
    }

    public void setTableOfAbsenceService(TableOfAbsenceService tableOfAbsenceService) {
        this.tableOfAbsenceService = tableOfAbsenceService;
    }
}
