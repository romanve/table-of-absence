package ru.cbr.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.cbr.model.User;
import ru.cbr.service.TableOfAbsenceService;

import java.util.List;

/**
 * @author Roman Velikoselsky
 */

@RestController
public class UserController {
    private TableOfAbsenceService tableOfAbsenceService;

    @RequestMapping(value = "/add-user", method = RequestMethod.POST)
    public ResponseEntity<User> addUser(@RequestBody User user) {
        if (tableOfAbsenceService.addUser(user)) {
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(user, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/get-users-list", method = RequestMethod.GET)
    public ResponseEntity<List> getListOfUsers() {
        List userList = tableOfAbsenceService.getUsersList();

        if (userList.isEmpty()) {
            return new ResponseEntity<List>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List>(userList, HttpStatus.OK);
    }

    @RequestMapping(value = "/get-filtered-users-list", method = RequestMethod.POST)
    public ResponseEntity<List> getFilteredListOfUsers(@RequestBody User user) {
        List userList = tableOfAbsenceService.getFilteredUsersList(user);

        if (userList.isEmpty()) {
            return new ResponseEntity<List>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List>(userList, HttpStatus.OK);
    }

    @RequestMapping(value = "/update-user/{id}", method = RequestMethod.POST)
    public ResponseEntity<User> updateUser(@PathVariable("id") int id, @RequestBody User user) {
        User updatableUser = tableOfAbsenceService.getUserById(id);

        if (updatableUser != null) {
            updatableUser.setPosition(user.getPosition());
            updatableUser.setFullName(user.getFullName());

            tableOfAbsenceService.updateUser(updatableUser);

            return new ResponseEntity<User>(HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/remove-user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> removeUser(@PathVariable("id") int id) {
        if (tableOfAbsenceService.removeUser(id)) {
            return new ResponseEntity<User>(HttpStatus.OK);
        } else {
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
    }

    public void setTableOfAbsenceService(TableOfAbsenceService tableOfAbsenceService) {
        this.tableOfAbsenceService = tableOfAbsenceService;
    }
}
