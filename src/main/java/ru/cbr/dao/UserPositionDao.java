package ru.cbr.dao;

import ru.cbr.model.User;

import java.util.List;

/**
 * @author Roman Velikoselsky
 */

public interface UserPositionDao {

    boolean addUser(User user);

    boolean updateUser(User user);

    boolean removeUser(int id);

    User getUserById(int id);

    User getUserByName(String fullName);

    List getUsersList();

    List getFilteredUsersList(User user);
}
