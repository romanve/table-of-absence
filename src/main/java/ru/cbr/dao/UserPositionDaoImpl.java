package ru.cbr.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import ru.cbr.model.User;

import java.util.Collections;
import java.util.List;

/**
 * @author Roman Velikoselsky
 */

@Repository
public class UserPositionDaoImpl implements UserPositionDao {
    private static final Logger logger = LoggerFactory.getLogger(UserPositionDaoImpl.class);

    private SessionFactory sessionFactory;

    public boolean addUser(User user) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(user);
            session.flush();

            return true;
        } catch (Exception e) {
            logger.error("addUser error: " + e.getMessage());
            return false;
        }
    }

    public boolean updateUser(User user) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.update(user);

            return true;
        } catch (Exception e) {
            logger.error("Failed to update user: " + user.toString() + " " + e.getMessage());
            return false;
        }
    }

    public boolean removeUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = getUserById(id);

        if (user != null) {
            session.delete(user);
            return true;
        }

        return false;
    }

    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();

        return session.load(User.class, id);
    }

    public User getUserByName(String fullName) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM User WHERE fullName =:fullName");
        query.setParameter("fullName", fullName);

        List users = query.list();

        if (users.size() > 1) {
            logger.warn("More then one user found by name: " + fullName + "get first user in list");
            return (User) users.get(0);
        }

        if (!users.isEmpty()) {
            return (User) users.get(0);
        } else {
            logger.error("Not found user by name: " + fullName);
            return null;
        }
    }

    public List getUsersList() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM User");

        return query.list();
    }

    public List getFilteredUsersList(User user) {
        List result = Collections.emptyList();

        Session session = sessionFactory.getCurrentSession();
        String fullName = user.getFullName();
        String position = user.getPosition();

        if ((fullName == null || fullName.equals("")) && (position == null || position.equals(""))) {
            return getUsersList();
        }

        Query query = null;

        if (fullName != null && !fullName.equals("") && position != null && !position.equals("")) {
            query = session.createQuery("FROM User U WHERE U.fullName =:fullName AND U.position =:position");
            query.setParameter("fullName", fullName);
            query.setParameter("position", position);
        }

        if ((fullName != null && !fullName.equals("")) && (position == null || position.equals(""))) {
            query = session.createQuery("FROM User U WHERE U.fullName =:fullName");
            query.setParameter("fullName", fullName);

        }

        if (fullName == null || fullName.equals("")) {
            query = session.createQuery("FROM User U WHERE U.position =:position");
            query.setParameter("position", position);
        }

        return query != null ? query.list() : result;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
