package ru.cbr.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import ru.cbr.model.TableOfAbsence;
import ru.cbr.model.User;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Roman Velikoselsky
 */

public class TableOfAbsenceDaoImpl implements TableOfAbsenceDao {
    private static final Logger logger = LoggerFactory.getLogger(TableOfAbsenceDaoImpl.class);

    private SessionFactory sessionFactory;
    private UserPositionDao userPositionDao;

    public boolean addAbsence(TableOfAbsence tableOfAbsence) {
        try {
            Session session = sessionFactory.getCurrentSession();
            tableOfAbsence.setUser(userPositionDao.getUserById(tableOfAbsence.getUserId()));
            session.persist(tableOfAbsence);
            session.flush();

            return true;
        } catch (Exception e) {
            logger.error("addAbsence error: " + e.getMessage());
            return false;
        }
    }

    public boolean updateAbsence(TableOfAbsence tableOfAbsence) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.update(tableOfAbsence);

            return true;
        } catch (Exception e) {
            logger.error("Failed to update absence: " + tableOfAbsence.toString() + " " + e.getMessage());
            return false;
        }
    }

    public boolean removeAbsence(int id) {
        Session session = sessionFactory.getCurrentSession();
        TableOfAbsence tableOfAbsence = getAbsenceById(id);

        if (tableOfAbsence != null) {
            session.delete(tableOfAbsence);
            return true;
        }

        return false;
    }

    public List getAbsenceListByUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        int id = user.getId();

        Query query = session.createQuery("FROM TableOfAbsence WHERE userId =:id");
        query.setParameter("id", id);

        return query.list();
    }

    public List getAbsenceList() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM TableOfAbsence t JOIN t.user u");

        return query.list();
    }

    public TableOfAbsence getAbsenceById(int id) {
        Session session = sessionFactory.getCurrentSession();

        return session.load(TableOfAbsence.class, id);
    }

    public List getFilteredTableOfAbsenceList(TableOfAbsence tableOfAbsence, User user, String position) {
        Session session = sessionFactory.getCurrentSession();
        StringBuilder queryBuilder = new StringBuilder();
        Map<String, Serializable> props = new HashMap<String, Serializable>();

        queryBuilder.append("FROM TableOfAbsence t JOIN t.user u WHERE ");

        if (user != null) {
            if (!isWhereEndsInString(queryBuilder.toString())) {
                queryBuilder.append(" AND ");
            }
            queryBuilder.append("u.id =:id");
            props.put("id", user.getId());
        }

        if (!StringUtils.isEmpty(position) && !"undefined".equals(position)) {
            if (!isWhereEndsInString(queryBuilder.toString())) {
                queryBuilder.append(" AND ");
            }
            queryBuilder.append("u.position =:position");
            props.put("position", position);
        }

        if (tableOfAbsence.getDateOfAbsence() != null) {
            if (!isWhereEndsInString(queryBuilder.toString())) {
                queryBuilder.append(" AND ");
            }

            queryBuilder.append("t.dateOfAbsence =:dateOfAbsence");
            props.put("dateOfAbsence", tableOfAbsence.getDateOfAbsence());
        }

        if (tableOfAbsence.getReasonOfAbsence() != null && !tableOfAbsence.getReasonOfAbsence().equals("")) {
            if (!isWhereEndsInString(queryBuilder.toString())) {
                queryBuilder.append(" AND ");
            }

            queryBuilder.append("t.reasonOfAbsence =:reasonOfAbsence");
            props.put("reasonOfAbsence", tableOfAbsence.getReasonOfAbsence());
        }

        if (tableOfAbsence.getTimeOfAbsence() > 0) {
            if (!isWhereEndsInString(queryBuilder.toString())) {
                queryBuilder.append(" AND ");
            }

            queryBuilder.append("t.timeOfAbsence =:timeOfAbsence");
            props.put("timeOfAbsence", tableOfAbsence.getTimeOfAbsence());
        }

        if (isWhereEndsInString(queryBuilder.toString())) {
            return getAbsenceList();
        }

        Query query = session.createQuery(queryBuilder.toString());

        for (Map.Entry<String, Serializable> prop : props.entrySet()) {
            query.setParameter(prop.getKey(), prop.getValue());
        }

        return query.list();
    }

    private boolean isWhereEndsInString(String string) {
        return string.endsWith(" WHERE ");
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void setUserPositionDao(UserPositionDao userPositionDao) {
        this.userPositionDao = userPositionDao;
    }
}
