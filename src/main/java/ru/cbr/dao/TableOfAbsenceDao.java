package ru.cbr.dao;

import ru.cbr.model.TableOfAbsence;
import ru.cbr.model.User;

import java.util.List;

/**
 * @author Roman Velikoselsky
 */

public interface TableOfAbsenceDao {

    boolean addAbsence(TableOfAbsence tableOfAbsence);

    boolean updateAbsence(TableOfAbsence tableOfAbsence);

    boolean removeAbsence(int id);

    TableOfAbsence getAbsenceById(int id);

    List getAbsenceListByUser(User user);

    List getAbsenceList();

    List getFilteredTableOfAbsenceList(TableOfAbsence tableOfAbsence, User user, String position);
}
