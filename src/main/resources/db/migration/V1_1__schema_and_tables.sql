create table absence.user_position
(
  user_full_name varchar(255) not null,
  user_position varchar(255) not null,
  user_id serial not null
  constraint user_position_user_id_pk
  primary key
)
;

create table absence.table_of_absence
(
  user_in_absence_id serial not null
  constraint table_of_absence_user_position_user_id_fk
  references absence.user_position,
  time_of_absence bigint not null,
  date_of_absence date not null,
  reason_of_absence text not null,
  absence_id serial not null
)
;