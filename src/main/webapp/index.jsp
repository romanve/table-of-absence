<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="tableOfAbsenceApp">
<head>
    <meta charset="utf-8">
    <title>Table Of Absence</title>
    <link rel="stylesheet" href="./resources/css/style.css">
    <script defer src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script defer src="./resources/js/app.js"></script>
</head>
<body>
<div class=wrapper>

    <header>
        <div class="header_title">
            <h1>Пользователи</h1>
        </div>
    </header>

    <div class="main_content" ng-controller="tableOfAbsenceCtrl">
        <aside>
            <div class="filtr_block_title">Фильтр</div>

            <div class="filtr_control_block">
                <p><div class="form_add_input">ФИО:<input type="text" size ="35" ng-model="form_add_filter_username"></div></p>
                <p><div class="form_add_input">Должность:<input type="text" size ="25" ng-model="form_add_filter_position"></div></p>
                <div class="user_button_ready" ng-click="getFilteredUsers()"></div>
            </div>
        </aside>


        <article>
            <div class="user_list_area">
                <div class="user_list_content">

                    <div ng-repeat="user in users | startFrom: startingUser() | limitTo: usersPerPage">
                        <div class="user">
                            <div class="user_input_field"><input id="user.fullName" type="text" size ="50" ng-model="user.fullName"></div>
                            <div class="user_input_field"><input id="user.position" type="text" size ="50" ng-model="user.position"></div>
                            <div class="user_button_ready" ng-click="updateUser(user)"></div>
                            <div class="user_button_trash" ng-click="removeUser(user)"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="pagination_area">
                <div class="pagination_to_left_button" ng-click="pageBackOfUsers()"></div>
                <div class="pagination_text">{{currentPageOfUsers+1}} из {{numberOfPagesOfUsers()}}</div>
                <div class="pagination_to_right_button" ng-click="pageForwardOfUsers()"></div>
            </div>

            <div class=add_user_area>
                <div class=add_user_title>Добавить нового пользователя</div>
                <div class=add_user_content>
                    <div class="add_user_form">
                        <div class="form_add_input">ФИО:<input type="text" size ="35" ng-model="form_add_username"></div>
                        <div class="form_add_input">Должность:<input type="text" size ="25" ng-model="form_add_position"></div>
                        <div class="form_add_button" ng-click="addUser()"></div>
                    </div>
                </div>
            </div>

        </article>
    </div>

</div>
<br/><br/><br/><br/><br/><br/><br/><br/>
<div class=wrapper>

    <header>
        <div class="header_title">
            <h1>Таблица отсутствия</h1>
        </div>
    </header>

    <div class="main_content" ng-controller="tableOfAbsenceCtrl">
        <aside>
            <div class="filtr_block_title">Фильтр</div>

            <div class="filtr_control_block">
                <p><div class="form_add_input">ФИО:<input type="text" size ="35" ng-model="form_add_filter_absence_username"></div></p>
                <p><div class="form_add_input">Должность:<input type="text" size ="25" ng-model="form_add_filter_absence_position"></div></p>
                <p><div class="form_add_input">Причина отсутствия:<input type="text" size ="25" ng-model="form_add_filter_absence_reason"></div></p>
                <p><div class="form_add_input">Дата отсутствия:<input type="date" size ="25" ng-model="form_add_filter_absence_date"></div></p>
                <p><div class="form_add_input">Время отсутствия:<input type="number" size ="25" ng-model="form_add_filter_absence_time"></div></p>
                <div class="user_button_ready" ng-click="getFilteredAbsence()"></div>
            </div>
        </aside>


        <article>
            <div class="user_list_area">
                <div class="user_list_content">

                    <div ng-repeat="absence in absences | startFrom: startingAbsence() | limitTo: absencePerPage">
                        <div class="user">
                            <div class="user_input_field">
                                <label ng-bind="absence[0].user.fullName"></label>
                                <label ng-bind="absence[0].user.position"></label>
                                <label ng-bind="absence[0].reasonOfAbsence"></label>
                                <label ng-bind="absence[0].dateOfAbsence | date:'dd/MM/yyyy'"></label>
                                <label ng-bind="absence[0].timeOfAbsence"></label>
                            </div>
                            <div class="user_button_trash" ng-click="removeAbsence(absence[0])"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="pagination_area">
                <div class="pagination_to_left_button" ng-click="pageBackOfAbsence()"></div>
                <div class="pagination_text">{{currentPageOfAbsence+1}} из {{numberOfPagesOfAbsence()}}</div>
                <div class="pagination_to_right_button" ng-click="pageForwardOfAbsence()"></div>
            </div>

            <div class=add_user_area>
                <div class=add_user_title>Добавить запись об отсутствии</div>
                <div class=add_user_content>
                    <div class="add_user_form">
                        <label for="selectUser">Сотрудник:</label>
                        <select name="selectUser" id="selectUser" ng-model="user.id">
                            <option ng-repeat="user in users" value="{{user.id}}">{{user.fullName}}</option>
                        </select>
                        <div class="form_add_input">Причина отсутствия:<input type="text" size ="25" ng-model="form_add_reason_of_absence"></div>
                        <div class="form_add_input">Дата отсутствия:<input type="date" size ="25" ng-model="form_add_date_of_absence"></div>
                        <div class="form_add_input">Время отсутствия (минуты):<input type="number" size ="25" ng-model="form_add_time_of_absence"></div>
                        <div class="form_add_button" ng-click="addAbsence()"></div>
                    </div>
                </div>
            </div>

        </article>
    </div>

</div>
</body>
</html>

