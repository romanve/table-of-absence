var tableOfAbsenceModule = angular.module('tableOfAbsenceApp', []);

tableOfAbsenceModule.controller("tableOfAbsenceCtrl", function ($scope, $http) {
    $scope.users = [];
    $scope.absences = [];
    $scope.currentAbsenceUser = null;
    $http.defaults.headers.post["Content-Type"] = "application/json; charset=utf-8";

    //---------------------  Pagination -------------------------
    $scope.currentPageOfUsers = 0;
    $scope.usersPerPage = 5;

    $scope.currentPageOfAbsence = 0;
    $scope.absencePerPage = 5;

    $scope.countUsers = function () {
        return $scope.users.length;
    };

    $scope.countAbsence = function () {
        return $scope.absences.length;
    };

    $scope.firstPageOfUsers = function () {
        return $scope.currentPageOfUsers === 0;
    };

    $scope.firstPageOfAbsence = function () {
        return $scope.currentPageOfAbsence === 0;
    };

    $scope.lastPageOfUsers = function () {
        var lastPageNum = Math.ceil($scope.countUsers() / $scope.usersPerPage - 1);
        return $scope.currentPageOfUsers === lastPageNum;
    };

    $scope.lastPageOfAbsence = function () {
        var lastPageNum = Math.ceil($scope.countUsers() / $scope.absencePerPage - 1);
        return $scope.currentPageOfAbsence === lastPageNum;
    };

    $scope.numberOfPagesOfUsers = function () {
        return Math.ceil($scope.countUsers() / $scope.usersPerPage);
    };

    $scope.numberOfPagesOfAbsence = function () {
        return Math.ceil($scope.countAbsence() / $scope.absencePerPage);
    };

    $scope.startingUser = function () {
        return $scope.currentPageOfUsers * $scope.usersPerPage;
    };

    $scope.startingAbsence = function () {
        return $scope.currentPageOfAbsence * $scope.absencePerPage;
    };

    $scope.pageBackOfUsers = function () {
        if ($scope.currentPageOfUsers > 0) {
            $scope.currentPageOfUsers = $scope.currentPageOfUsers - 1;
        }
    };

    $scope.pageBackOfAbsence = function () {
        if ($scope.currentPageOfAbsence > 0) {
            $scope.currentPageOfAbsence = $scope.currentPageOfAbsence - 1;
        }
    };

    $scope.pageForwardOfUsers = function () {
        if ($scope.currentPageOfUsers < $scope.numberOfPagesOfUsers() - 1) {
            $scope.currentPageOfUsers = $scope.currentPageOfUsers + 1;
        }
    };

    $scope.pageForwardOfAbsence = function () {
        if ($scope.currentPageOfAbsence < $scope.numberOfPagesOfAbsence() - 1) {
            $scope.currentPageOfAbsence = $scope.currentPageOfAbsence + 1;
        }
    };

    //---------------------------------------------------------------

    $http({
        method: "GET",
        url: "/get-users-list",
        headers: {"Content-Type": "application/json; charset=utf-8"}
    }).then(function successCallback(response) {
        $scope.users = response.data;
    }, function errorCallback(response) {
        alert("Ошибка " + response.toString());
    });

    $http({
        method: "GET",
        url: "/get-absence-list",
        headers: {"Content-Type": "application/json; charset=utf-8"}
    }).then(function successCallback(response) {
        $scope.absences = response.data;
    }, function errorCallback(response) {
        alert("Ошибка " + response.toString());
    });

    $scope.getFilteredUsers = function getFilteredUsers() {
        var filteredUser = {
            "fullName": $scope.form_add_filter_username,
            "position": $scope.form_add_filter_position,
            "id": 0
        };

        $http({
            method: "POST",
            url: "/get-filtered-users-list",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            data: filteredUser
        }).then(function successCallback(response) {
            $scope.users = response.data;
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.getFilteredAbsence = function getFilteredAbsence() {
        var filteredAbsence = {
            "reasonOfAbsence": $scope.form_add_filter_absence_reason,
            "dateOfAbsence": $scope.form_add_filter_absence_date,
            "timeOfAbsence": $scope.form_add_filter_absence_time
        };

        $http({
            method: "POST",
            url: "/get-filtered-absence-list/" + $scope.form_add_filter_absence_username + "&" + $scope.form_add_filter_absence_position,
            headers: {"Content-Type": "application/json; charset=utf-8"},
            data: filteredAbsence
        }).then(function successCallback(response) {
            $scope.absences = response.data;
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.addUser = function addUser() {
        var newUser = {
            "fullName": $scope.form_add_username,
            "position": $scope.form_add_position,
            "id": 0
        };

        if (!newUser.fullName || !newUser.position) {
            alert("ФИО и должность - обязательны к заполнению");
            return;
        }

        $http({
            method: "POST",
            url: "/add-user",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            data: newUser
        }).then(function successCallback(response) {
            alert("Пользователь добавлен");
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.updateUser = function updateUser(user) {
        if (!user.fullName || !user.position) {
            alert("ФИО и должность - обязательны к заполнению");
            return;
        }

        var updatableUser = {
            "fullName": user.fullName,
            "position": user.position,
            "id": 0
        };

        $http({
            method: "POST",
            url: "/update-user/" + user.id,
            headers: {"Content-Type": "application/json; charset=utf-8"},
            data: updatableUser
        }).then(function successCallback(response) {
            alert("Пользователь изменен");
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.removeUser = function removeUser(user) {
        $http.delete("/remove-user/" + user.id, []).then(function successCallback(response) {
            var index = $scope.users.indexOf(user);
            $scope.users.splice(index, 1);
            alert("Пользователь удалён");
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.removeAbsence = function removeAbsence(absence) {
        $http.delete("/remove-absence/" + absence.absenceId, []).then(function successCallback(response) {
            var index = $scope.absences.indexOf(absence);
            $scope.absences.splice(index, 1);
            alert("Запись удалена");
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.addAbsence = function addAbsence() {
        var absence = {
            "userId": $scope.user.id,
            "timeOfAbsence": $scope.form_add_time_of_absence,
            "dateOfAbsence": $scope.form_add_date_of_absence,
            "reasonOfAbsence": $scope.form_add_reason_of_absence
        };

        if (!absence || !absence.dateOfAbsence || !absence.timeOfAbsence || !absence.userId || !absence.reasonOfAbsence) {
            alert("Все поля обязательны к заполнению");
            return;
        }

        $http({
            method: "POST",
            url: "/add-absence",
            headers: {"Content-Type": "application/json; charset=utf-8"},
            data: absence
        }).then(function successCallback(response) {
            alert("Запись добавлена");
        }, function errorCallback(response) {
            alert("Ошибка " + response.toString());
        });
    };

    $scope.getUserIdByFullName = function getUserIdByFullName(fullName) {
        var currentUser = "";

        angular.forEach($scope.users, function(user) {
            if (user.id === id) {
                currentUser = user.fullName;
            }
        });

        return currentUser;
    };

    $scope.getUserFullNameById = function getUserFullNameById(id) {
        var currentUser = "";

        angular.forEach($scope.users, function(user) {
            if (user.id === id) {
                currentUser = user.fullName;
            }
        });

        return currentUser;
    };

    $scope.getUserPositionById = function getUserPositionById(id) {
        var currentUser = "";

        angular.forEach($scope.users, function(user) {
            if (user.id === id) {
                currentUser = user.position;
            }
        });

        return currentUser;
    };
});

tableOfAbsenceModule.filter('startFrom', function () {
    return function (input, start) {
        start = +start;
        return input.slice(start);
    }
});