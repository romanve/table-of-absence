Connection to postgreSQL:
jdbc:postgresql://localhost:5432/postgres - replace to custom in:
beans: dataSource, hibernate5AnnotatedSessionFactory
maven: flyway-maven-plugin
login/pass: postgres/postgres (dataSource bean, flywaydb plugin)

Migration:
mvn flyway:migrate
(create schema and tables, resources/db/migration/V1_1__schema_and_tables.sql)

Build and deploy:
mvn tomcat7:run-war

Фильтр в таблице отсутствия по ФИО и Должности работает только на англ. значениях.
Не успел подружить с русским. Остальные фильтры работают независимо от языка.